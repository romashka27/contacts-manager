import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { contacts } from '../contacts.json';

class AddForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: ''
    }
  }
  
  onNameChange = (event) => {
    this.setState({ name: event.target.value })
  }

  onEmailChange = (event) => {
    this.setState({ email: event.target.value })
  }

  onAddContact = () => {
    fetch('http://localhost:3000/add', {
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            name: this.state.name,
            email: this.state.email,
        })
    })
    .then(response => response.json())
    .then(contact => {
        if(contact){
            contacts.unshift(contact)
            this.props.loadContact(contact)
            this.props.onRouteChange('home'); 
        }
    })
}
  render() {
    return (
      <form style={{display: 'flex', flexDirection: 'column', width: 400, margin: '0 auto', marginTop: '5em'}} noValidate autoComplete="off">
        <TextField
          id="name"
          label="Name"
          style={{}}
          margin="normal"
          onChange={this.onNameChange}
        />
        <TextField
          id="email"
          label="Email"
          style={{}}
          margin="normal"
          onChange={this.onEmailChange}
        />
          <Button variant="contained" color="secondary" 
          style={{marginTop: "3em"}}
          onClick={this.onAddContact}>
          Add contact
        </Button>
      </form>
    );
  }
  
}

export default AddForm;